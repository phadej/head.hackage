# GHC/head.hackage CI support
#
# See .gitlab-ci.yml in the root of this repository.

# Expects to be run on x86_64.
# bindistTarball should be a fedora27 tarball.
{ bindistTarball
, extraHcOpts ? ""
}:

let
  # GHC from the given bindist.
  ghc =
    let
      commit = "027df18eb894ef622f67b2f487a851dc534403e3";
      src = fetchTarball {
        url = "https://github.com/bgamari/ghc-artefact-nix/archive/${commit}.tar.gz";
        sha256 = "0ijdpqsg8b7hz89hpgmpzfvg70y6yfq4wrqgngvqlfri5hy9fmg3";
      };
    in nixpkgs.callPackage "${src}/artifact.nix" {} {
      ncursesVersion = "6";
      bindistTarball =
        if builtins.typeOf bindistTarball == "path"
        then bindistTarball
        else builtins.fetchurl bindistTarball;
    };

  # Build configuration.
  nixpkgs = import ../. {
    ghc = self: ghc;
    haskellOverrides = self: super: rec {
      mkDerivation = drv: super.mkDerivation (drv // {
        doHaddock = false;
        enableLibraryProfiling = false;
        enableExecutableProfiling = false;
        hyperlinkSource = false;
        broken = false;
        configureFlags = (drv.configureFlags or []) ++ [
          "--ghc-options=-ddump-timings"
          "--ghc-options=\"${extraHcOpts}\""
        ];
      });
    };
  };

  # Build dependencies worth caching.
  buildDepends = with nixpkgs; [
    buildPackages.haskellPackages.jailbreak-cabal
    buildPackages.haskellPackages.cabal2nix
  ];

  # Get build-time dependencies of a derivation.
  # See https://github.com/NixOS/nix/issues/1245#issuecomment-401642781
  getDepends = pkg: let
    drv = builtins.readFile pkg.drvPath;
    storeDirRe = lib.replaceStrings [ "." ] [ "\\." ] builtins.storeDir;
    storeBaseRe = "[0-9a-df-np-sv-z]{32}-[+_?=a-zA-Z0-9-][+_?=.a-zA-Z0-9-]*";
    re = "(${storeDirRe}/${storeBaseRe}\\.drv)";
    inputs = lib.concatLists (lib.filter lib.isList (builtins.split re drv));
  in map import inputs;

  # All Haskell packages built with the HEAD compiler.
  haskellPackages = nixpkgs.haskellPackages.extend(sel: sup: {terminfo = null;});

  # The packages which we are here to test
  testedPackages = with nixpkgs.haskell.lib; with haskellPackages; {
    inherit lens aeson criterion scotty generic-lens;

    # servant: Don't distribute Sphinx documentation
    servant = overrideCabal servant { postInstall = ""; };
    # singletons: Disable testsuite since it often breaks due to spurious
    # TH-pretty-printing changes
    singletons = dontCheck singletons;
  } // patchedPackages;

  # All of the packages which we have patches for.
  patchedPackages =
    let
      toPackageAttr = patchFile: _type:
        let
          parts = builtins.match "([a-zA-Z0-9-]+)-.*" patchFile;
          pkgName = builtins.elemAt parts 0;
        in lib.nameValuePair pkgName haskellPackages."${pkgName}";
    in lib.mapAttrs' toPackageAttr (builtins.readDir ../patches);

  inherit (nixpkgs) lib;

  transHaskellDeps = drv:
    if drv == null then [] else
    let
      haskellDeps = drv.passthru.getBuildInputs.haskellBuildInputs or [];
    in [{
      haskellDeps = map (dep: dep.name) haskellDeps;
      drvName = drv.name;
      drvPath = drv.drvPath;
      out = drv.outPath;
      name = drv.passthru.pname;
      version = drv.passthru.version;
    }] ++ lib.concatMap transHaskellDeps haskellDeps;

  # Used by the summarize.py script to get a picture of the packages built during the run.
  summary = {
    pkgs = lib.concatMap transHaskellDeps (lib.attrValues testedPackages);
    roots = lib.concatMap (drv: if drv == null then [] else [drv.name]) (lib.attrValues testedPackages);
  };

  # Find Job ID of the given job name in the given pipeline
  find-job = nixpkgs.writeScriptBin "find-job.sh" ''
    set -e

    project_id=$1
    pipeline_id=$2
    job_name=$3

    # Access token is a protected environment variable in the head.hackage project and
    # is necessary for this query to succeed. Sadly job tokens only seem to
    # give us access to the project being built.
    ${nixpkgs.curl}/bin/curl \
      --silent --show-error \
      -H "Private-Token: $ACCESS_TOKEN" \
      "https://gitlab.haskell.org/api/v4/projects/$project_id/pipelines/$pipeline_id/jobs?scope[]=success" \
      > resp.json

    job_id=$(${nixpkgs.jq}/bin/jq ". | map(select(.name == \"$job_name\")) | .[0].id" < resp.json)
    if [ "$job_id" = "null" ]; then
      echo "Error finding job $job_name for $pipeline_id in project $project_id:" >&2
      cat resp.json >&2
      rm resp.json
      exit 1
    else
      rm resp.json
      echo -n "$job_id"
    fi
  '';

in {
  inherit testedPackages patchedPackages;
  inherit nixpkgs ghc buildDepends;
  inherit haskellPackages;
  testedPackageNames = nixpkgs.lib.attrNames testedPackages;
  inherit summary find-job;
}
