{ nixpkgs ? (import <nixpkgs> {}) }:

with nixpkgs;
let
  hackage-repo-tool =
    let
      src = fetchFromGitHub {
        owner = "haskell-vanguard";
        repo = "hackage-security";
        rev = "5ce34d42ffa9d760dafcf216a10d6f72bd82a1d3";
        sha256 = "07a6f9gl4xn4fpc09wypm29cwghx31dw0lzq8421v9fjb5m2r96w";
      };
    in haskellPackages.callCabal2nix "hackage-repo-tool" "${src}/hackage-repo-tool" {};

  overlay-tool =
    let
      src = fetchFromGitHub {
        owner = "bgamari";
        repo = "hackage-overlay-repo-tool";
        rev = "40282de72ebd4158bfca677b8fa179ed74860e68";
        sha256 = "1z9yy63a9l149in3cb42cylpp1mw70smqh6hrp7n15n3zjfa1w1x";
      };
    in haskellPackages.callCabal2nix "hackage-overlay-repo-tool" src {};

  build-repo =
    let
      deps = [
        bash curl gnutar findutils patch rsync openssl
        cabal-install ghc
        hackage-repo-tool overlay-tool
      ];
    in runCommand "repo" {
    nativeBuildInputs = [ makeWrapper ];
  } ''
    mkdir -p $out/bin
    makeWrapper ${./build-repo.sh} $out/bin/build-repo.sh \
        --prefix PATH : ${stdenv.lib.makeBinPath deps}
  '';
in
  build-repo
